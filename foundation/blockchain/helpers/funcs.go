package helpers

import (
	"crypto/ecdsa"

	"github.com/ethereum/go-ethereum/crypto"

	"gitlab.com/david-kalmakoff/blockchain/foundation/blockchain/database"
)

// GeneratePrivateKey creates a private key
func GeneratePrivateKey() (*ecdsa.PrivateKey, error) {
	privateKey, err := crypto.GenerateKey()
	if err != nil {
		return nil, err
	}

	return privateKey, nil
}

// GetAddressFromPrivateKey gets the public key string from the private key
func GetAddressFromPrivateKey(key *ecdsa.PrivateKey) (database.AccountID, error) {
	addr, err := database.ToAccountID(crypto.PubkeyToAddress(key.PublicKey).String())
	if err != nil {
		return "", err
	}

	return addr, nil
}

// GetECDSAFromPrivateKey gets byte array version of the private key
func GetECDSAFromPrivateKey(key *ecdsa.PrivateKey) []byte {
	return crypto.FromECDSA(key)
}
